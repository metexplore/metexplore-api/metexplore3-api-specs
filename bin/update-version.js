#!/usr/bin/env node


const fs = require('fs');
const { execSync } = require('child_process');

// Récupérer l'argument de ligne de commande correspondant à la version
const versionArg = process.argv[2];

let newVersion;


newVersion = execSync(`npm version --no-git-tag-version ${versionArg}`).toString().trim();


// Lire le fichier package.json
const packageJson = JSON.parse(fs.readFileSync('package.json', 'utf8'));

// Mettre à jour la version dans le fichier YAML
const openapiFile = 'metexplore3-api.yml';

// Lire le contenu du fichier YAML
let openapiContent = fs.readFileSync(openapiFile, 'utf8');

// Remplacer la version dans le contenu du fichier YAML
const updatedOpenapiContent = openapiContent.replace(/version: (.+)/, `version: ${packageJson.version}`);

// Écrire le contenu mis à jour dans le fichier YAML
fs.writeFileSync(openapiFile, updatedOpenapiContent, 'utf8');

execSync(`git add package.json ${openapiFile}`);
execSync(`git commit -m "Update version of package and yaml files to ${newVersion}"`);
execSync(`git tag ${newVersion}`);


console.log(`Successfully updated versions of package.json and ${openapiFile} to version ${newVersion}`);
