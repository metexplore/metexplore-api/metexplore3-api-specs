# MetExplore 3 Api

You have just to edit the yaml files and push to master to launch ci jobs that will create doc and npm package.

The following sections explain the different actions done in CI if you want to test them before pushing your code.

The last sections explain how to access the yaml file via npm.

## Install

```bash
npm install
```

## Validation

```bash
npm run validate
```

## Merge Yaml file

```bash
npm run build
```

Create a new yaml file in the build directory.

## Validation of the bundle file

```bash
npm run validate-build
```

Automatically done in CI.

## Documentation generation

Need the bundle file built with 'npm run build'.

```bash
npm run generate-doc
```

Automatically done in CI and available in <https://metexplore.pages.mia.inra.fr/metexplore-api/metexplore3-api-specs/> .

## Change version

Major version (X.y.z):

```bash
npm run update-version major
```

Minor version (x.Y.z):

```bash
npm run update-version minor
```

Patch version (x.y.Z):

```bash
npm run update-version patch
```

To a specific version:

```bash
npm run update-version 0.6.0
```

The command "npm run update-version" changes both package.json and metexplore3-api.yml files.

## Npm publish

Automatically done in CI.
Published on <https://forgemia.inra.fr/metexplore/metexplore-api/metexplore3-api-specs/-/packages>

## How to import yaml file from npm

Create a .npmrc file:

```.npmrc
@metexplore:registry=https://forgemia.inra.fr/api/v4/packages/npm/
//forgemia.inra.fr/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}
```

Install the npm module:

```bash
export CI_JOB_TOKEN=cBZySZds******
npm install --save @metexplore/metexplore3-api-specs
```

Use it in your code:

```typescript
const yamlFile = 'node_modules/@metexplore/metexplore3-api-specs/build/metexplore3-api.yml';
```
